exports.FixMongoBin=function FixMongoBin(object){//fix mongodb's horable binary type
  var keys=Object.keys(object);
  for(var i=0,l=keys.length;i<l;i++){
    if(typeof object[keys[i]] === 'object'){
      if(object[keys[i]]._bsontype==='Binary')
        object[keys[i]]=object[keys[i]].buffer;
      else
        FixMongoBin(object[keys[i]]);
    }
  }
};