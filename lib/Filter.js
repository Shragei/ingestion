var util=require('util');
var events=require('events');
var stream=require('stream');
var buffer=require('buffer');
var lodash=require('lodash');
function Filter(query){
  if(options===undefined)
    options={};
  options.objectMode=true;
  stream.Transform.call(this,options);
  
  var filterParams=parse(query);
  var color=undefined;
 
  this.stringify=function stringify(){
    return query;
  };
  
  this._transform=function Transform(chunk,encoding,cb){
    var push=function(keep){
      if(color!==undefined){
        if(keep)
          chunk.Color=color;
        this.push(chunk);
        cb();
      }else{
        if(keep)
          this.push(chunk);
        cb();
      }
    }.bind(this);
    
    
    if(filterParams.key!==null){
      val=lodash.get(filterParams.key);
      var push=true;
      if(val!==undefined){
        switch(filterParams.action){
          case 1:
            if(val===filterParams.val) break;
            else return push(false);
          case 2:
            if(val>filterParams.val) break;
            else return push(false);
          case 3:
            if(val>=filterParams.val) break;
            else return push(false);
          case 4:
            if(val<filterParams.val) break;
            else return push(false);
          case 5:
            if(val<=filterParams.val) break;
            else return push(false);
          case 6:
            var params=filterParams.val;
            var sub=val.slice(params.offset);
            var t=buffer.compare(params.val,sub);
            if(t==0||(params.search&&t>=0)) break;
            else return push(false);
        }
        push(true);
      }
    }else
      push(true);
    
    push(false);
  };
}

function prase(query){
  var key=query.match(/[^\!=\>\<\{\-]+/)[0];
  var val;
  var action=0;

  var isString=/(["'])((?:(?=(?:\\?))\2.)*?\1)/;
  var actions=[
    [/==/,1],
    [/>=/,3],
    [/<=/,5],
    [/(?:\((\d+)\))?->\[((?:[0-9a-f]{2}\s)*[0-9a-f]{2})\]/i,6]
    [/>/,2],
    [/</,4],
  ];
  function mapHex(val){
    return val.split(' ').map(function(el){return Number('0x'+el);});
  }
  if(key[0].length==0){
    return null;
  }else{
    key=key[0];
  }
  for(var i=0;i<actions.length;i++){
    var ret=query.match(actions[i][0]);
    if(ret){
      action=actions[i][1];
      if(ret.length>1){
        if(action==6){
          val={offset:0,hex:[]};
          if(ret.length===3){
            val.offset=Number(ret[1]);
            val.hex=mapHex(ret[2]);
          }else
            val.hex=mapHex(ret[1]);
        }else{
          val=ret.slice(1);
        }
      }else{
        var offset=ret[0].length+ret.index;
        val=query.substr(offset);
        var strMatch=val.match(isString);
        if(strMatch)
          val=strMatch[2];
      }
      break;
    }
  }
  return{
    key:key,
    action:action,
    val:val
  };
}


util.inherits(Filter,stream.Transform);
module.exports=Filter;