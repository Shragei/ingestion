var SocketIO=require('socket.io');
var Express=require('express');
var HTTP=require('http');
var Crypto=require('crypto');
var Path=require('path');
var Config=require('./config.json');

var Capture=new (require('./lib/Capture.js'))(Config.TargetMAC);
var Decoder=new (require('./lib/Decoder'))();
var Filer=require('./lib/Filter');

var Application=Express();
var Server=require('http').Server(Application)
var IO=SocketIO(Server);
var filterStack=[];
var Sink=new (require('stream').PassThrough)({objectMode:true});

Capture.pipe(Decoder);
IO.on('connection',function(socket){
  socket.on('StartCapture',function(path){
    var sessionId=Capture.Start();
    socket.emit('CaptureState',Capture.State);
  });
  
  socket.on('StopCapture',function(){
      Capture.Stop();
      socket.emit('CaptureState',Capture.State);
  });
  socket.on('GetCaptureStats',function(){
    socket.emit('CaptureState',Capture.State);
    socket.emit('CaptureStats',Capture.Stats());
  });
  
  socket.on('AddFilter',function(params){
    var filterId=Crypto.randomBytes(4).toString('base64');
    
    var filter=new Filter(params.query);
    if(params.color)
      filter.Color(color);
    var last=filterStack.length?filterStack[filterStack.length-1]:Decoder;
    last.unpipe(Sink);
    last.pipe(filter);
    filterStack.push(filter);
    filter.pipe(Sink);
    ListFilters();
  });
  
  socket.on('RemoveFilter',function(idx){
    if(idx<filterStack.length&&idx>=0){
      var before=idx==0?Decoder:filterStack[idx-1];
      var after=(idx+1)<filterStack.length?filterStack[idx+1]:Sink;
      var cur=filterStack.splice(idx)[0];
      before.unpipe(cur);
      cur.unpipe(after);
      before.pipe(after);
    }
    ListFilters();
  });
  
  socket.on('ListFilters',ListFilters);
  
  socket.on('ClearFilters',ClearFilters);
  function ListFilters(){
    var list=[];
    for(var i=0;i<filterStack;i++)
      list.push(filterStack[i].stringify());
    socket.emit('Filters',list);
  }
  function ClearFilters(){
    var cur=Decoder;
    for(var i=0;i<filterStack.length;i++){
      var next=filterStack[i];
      cur.unpipe(next);
      cur=next;
    }
    cur.unpipe(Sink);
    filterStack=[];
  }
  function SendPacket(packet){
    socket.emit('Packet',packet);
  }
  Sink.on('data',SendPacket)
  socket.on('disconnect',function(){
    ClearFilters();
    Sink.removeListener('data',SendPacket);
  })
});

Application.use('/resources',Express.static(Path.join(__dirname,'resources')));
Application.get('/',function(req,res){
  res.sendFile(Path.join(__dirname,'index.html'));
});
var address=Config.Address.split(':');
Server.listen(address[1],address[0]);
