'use strict';
var angular=require('angular');

var ip=angular.module('ip',[]);
ip.controller('ipCtrl',['$scope',function($scope){
  $scope.connections=[];
  $scope.selected=undefined;
  
  $scope.Select=function(ip){
    if(ip==$scope.selected)
      $scope.selected==undefined;
    else
      $scope.selected=ip;
    $scope.$emit('Reflector',{Event:'LimitToConnection',From:'IP.Select',Data:$scope.selected});
  }
  
  $scope.$on('Connections',function(event,data){
    if(data.indexOf($scope.selected)<0){
      $scope.selected==undefined;
      $scope.$emit('Reflector',{Event:'LimitToConnection',From:'IP.Select',Data:$scope.selected});
    }
    $scope.connections=data;             
  });
}])