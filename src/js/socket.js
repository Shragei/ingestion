var io=require('socket.io-client');
var Angular=require('angular');

var socket=angular.module('socket',[]);
socket.factory('socket',function($rootScope){
  var socket=io();
  return{
    on:function(eventName,callback){
      socket.on(eventName,function(){  
        var args=arguments;
        $rootScope.$apply(function(){
          callback.apply(socket,args);
        });
      });
    },
    emit:function(eventName,data,callback){
      socket.emit(eventName,data,function(){
        var args=arguments;
        $rootScope.$apply(function(){
          if(callback){
            callback.apply(socket,args);
          }
        });
      });
    }
  };
});