'use strict';
require('./ui'); //hopefully angular2 plays nicer with browserify
require('./socket');
var $=require('jquery');

var Angular=require('angular');
var main=Angular.module('main',['ui','socket']);

main.run(['$rootScope'],function($rootScope){
  $rootScope.$on('Reflector',function(event,data){
    console.group('Reflector');
    console.log("Event: "+data.Event);
    console.log("From: "+data.From);
    console.group("Payload");
    console.dir(data.Payload);
    console.groupEnd();
    console.groupEnd();
    $rootScope.$broadcast(data.Event,data.Payload);
  });
});
main.controller('main',['$scope',function($scope){
  
}]);

main.controller('PacketFeed',['$scope',function($scope){
  var decoderDeps={};
  var even=true;
  $scope.$on('Decoders',function(decoders){
    decoderDeps={};
    for(var i=0;i<decoders.length;i++){
      var depends=decoders[i].Depends;
      if(!(depends in decoderDeps))
        decoderDeps[depends]=[];
      decoderDeps[depends].push(decoders[i].Name);
    }
  });
  $scope.$on('Packet',function(packet){
    var tree=['Frame'];
    var last=tree[tree.length-1];
    var found=true;
    var address='';
    var direction='';
    var color=packet.Color;
    
    if(packet.IP){
      if(packet.Direction==='Inbound'){
        address=packet.IP.SourceAddress;
        direction='>>';
      }else{
        address=packet.IP.DestinationAddress;
        direction='<<';
      }
    }
    do{
      if(last in decoderDeps){
        var available=decoderDeps[last];
        found=false;
        for(var i=0;i<available.length;i++){
          if(available[i] in packet){
            found=true;
            tree.push(available[i]);
            packet=packet[available[i]];
          }
        }
      }else
        found=false;
    }while(found)
    
    tree.shift();
    
    if(color===undefined)
      color=even?'#1d191a':'#2f282a';
    even=!even;
    
    $scope.feed.push({
      color:color,
      message:addres+' '+direction+' '+tree.join(' ➤ ')
    });
    if($scope.feed.length>20)
      $scope.feed.shift();
  })
  $scope.feed=[];
}]);