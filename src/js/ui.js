var $=require('jquery');
var Angular=require('angular');


var ui=Angular.module('ui',[]);


ui.directive('uiTree',function(){
  return{
    restrict:'A',
    scope:{
      toggle:'=uiToggle',
    },
    link:function($scope,$element,$attr){
      if(!('disableInteraction' in $attr)){
        $scope.toggle=false;
        $element.on('click',function(){
          $scope.$apply(function(){
            $scope.toggle=!$scope.toggle;
          });
        })
      }
      
      $scope.$watch('toggle',function(val){
        if(val===true){
          $element.addClass('expand');
        }
        if(val===false){
          $element.removeClass('expand');
        }
      });
    }
  }
});