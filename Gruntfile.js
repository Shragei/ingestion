'use strict';
module.exports=function(grunt){
  grunt.initConfig({
    less:{
      dev:{
        options:{
          strictMath:true,
          sourceMap:true,
          outputSourceFiles:false
        },
        src:"src/less/main.less",
        dest:"resources/css/main.css"
      }
    },
    jade:{
      dev:{
        options:{
          pretty:true,
          data:{
            debug:true
          }
        },
        files:{
          "index.html":"src/jade/index.jade"
        }
      }
    },
    browserify:{
      dev:{
          options:{
          browserifyOptions:{
            debug:true
          }
        },
        files:{
          "resources/js/main.js":"src/js/main.js",
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.registerTask('default',['browserify:dev','jade:dev','less:dev']);
  grunt.registerTask('less-compile',['less:dev']);
  grunt.registerTask('jade-compile',['jade:dev']);
  grunt.registerTask('browserify-compile',['browserify:dev']);
};